use gotham::handler::IntoResponse;
use gotham::helpers::http::response::create_response;
use gotham::hyper::{Body, Response, StatusCode};
use gotham::router::builder::*;
use gotham::router::Router;
use gotham::state::{State, FromState};

#[derive(Deserialize, Clone, StateData, StaticResponseExtender)]
struct PathStringExtractor {
    name: String,
}

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
struct SomeStruct {
    id: i16,
    name: String,
    date: String,
}

impl IntoResponse for SomeStruct {
    fn into_response(self, state: &State) -> Response<Body> {
        create_response(
            state,
            StatusCode::OK,
            mime::APPLICATION_JSON,
            serde_json::to_string(&self).expect("serialized data"),
        )
    }
}

fn hello_body(mut state: State) -> (State, SomeStruct) {
    let path_param = PathStringExtractor::take_from(&mut state);
    (state, SomeStruct {
        id: 1,
        name: path_param.name.clone(),
        date: String::from("2020-08-11"),
    })
}

fn router() -> Router {
    build_simple_router(|route| {
        route.get("/hello/body/:name").with_path_extractor::<PathStringExtractor>().to(hello_body);
    })
}

pub fn main() {
    let addr = "127.0.0.1:7878";
    println!("Gotham server is running at http://{}", addr);
    gotham::start(addr, router())
}

/// #################################
/// ######       tests         ######
/// #################################

#[cfg(test)]
mod tests {
    use super::*;
    use gotham::hyper::StatusCode;
    use gotham::test::TestServer;

    #[test]
    fn receive_version_response() {
        let response = TestServer::new(router()).unwrap()
            .client()
            .get("http://localhost/hello/body/test")
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::OK);
        let res: SomeStruct = serde_json::from_str(&String::from_utf8(response.read_body().unwrap()).unwrap()).unwrap();
        assert_eq!(res, SomeStruct { id: 1, name: String::from("test"), date: String::from("2020-08-11"),  });
    }
}