#[macro_use]
extern crate gotham_derive;
#[macro_use]
extern crate serde_derive;

pub mod version;
pub mod router;
pub mod response;