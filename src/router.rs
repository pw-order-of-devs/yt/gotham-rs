use gotham::router::builder::*;
use gotham::router::Router;
use gotham::state::{State, FromState};

#[derive(Deserialize, Clone, StateData, StaticResponseExtender)]
struct QueryStringExtractor {
    name: String,
}

#[derive(Deserialize, Clone, StateData, StaticResponseExtender)]
struct PathStringExtractor {
    name: String,
}

fn version(state: State) -> (State, String) {
    (state, String::from("0.1.0"))
}
fn hello_query(mut state: State) -> (State, String) {
    let query_param = QueryStringExtractor::take_from(&mut state);
    (state, format!("Hello, {}", query_param.name.clone()))
}
fn hello_path(mut state: State) -> (State, String) {
    let path_param = PathStringExtractor::take_from(&mut state);
    (state, format!("Hello, {}", path_param.name.clone()))
}

fn router() -> Router {
    build_simple_router(|route| {
        route.get("/version").to(version);
        route.get("/hello").with_query_string_extractor::<QueryStringExtractor>().to(hello_query);
        route.get("/hello/:name").with_path_extractor::<PathStringExtractor>().to(hello_path);
    })
}

pub fn main() {
    let addr = "127.0.0.1:7878";
    println!("Gotham server is running at http://{}", addr);
    gotham::start(addr, router())
}

/// #################################
/// ######       tests         ######
/// #################################

#[cfg(test)]
mod tests {
    use super::*;
    use gotham::hyper::StatusCode;
    use gotham::test::TestServer;

    #[test]
    fn receive_version_response() {
        let response = TestServer::new(router()).unwrap()
            .client()
            .get("http://localhost/version")
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::OK);
        assert_eq!(&response.read_body().unwrap()[..], b"0.1.0");
    }

    #[test]
    fn receive_hello_query_response() {
        let response = TestServer::new(router()).unwrap()
            .client()
            .get("http://localhost/hello?name=test_name")
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::OK);
        assert_eq!(&response.read_body().unwrap()[..], b"Hello, test_name");
    }

    #[test]
    fn receive_hello_path_response() {
        let response = TestServer::new(router()).unwrap()
            .client()
            .get("http://localhost/hello/test_name")
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::OK);
        assert_eq!(&response.read_body().unwrap()[..], b"Hello, test_name");
    }
}