use futures::prelude::*;
use std::pin::Pin;

use gotham::handler::{HandlerFuture, IntoHandlerError};
use gotham::helpers::http::response::create_empty_response;
use gotham::hyper::{body, Body, StatusCode};
use gotham::router::builder::*;
use gotham::router::Router;
use gotham::state::{State, FromState};

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
struct SomeStruct {
    id: i16,
    name: String,
    date: String,
}

fn hello_body(mut state: State) -> Pin<Box<HandlerFuture>> {
    let f = body::to_bytes(Body::take_from(&mut state)).then(|full_body| match full_body {
        Ok(valid_body) => {
            let body_content = String::from_utf8(valid_body.to_vec()).unwrap();
            println!("Body: {}", body_content);
            let res = create_empty_response(&state, StatusCode::OK);
            future::ok((state, res))
        }
        Err(e) => future::err((state, e.into_handler_error())),
    });
    f.boxed()
}

fn router() -> Router {
    build_simple_router(|route| {
        route.post("/hello/req").to(hello_body);
    })
}

pub fn main() {
    let addr = "127.0.0.1:7878";
    println!("Gotham server is running at http://{}", addr);
    gotham::start(addr, router())
}

/// #################################
/// ######       tests         ######
/// #################################

#[cfg(test)]
mod tests {
    use super::*;
    use gotham::hyper::StatusCode;
    use gotham::test::TestServer;

    #[test]
    fn receive_version_response() {
        let response = TestServer::new(router()).unwrap()
            .client()
            .post("http://localhost/hello/req")
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::OK);
        let res: SomeStruct = serde_json::from_str(&String::from_utf8(response.read_body().unwrap()).unwrap()).unwrap();
        assert_eq!(res, SomeStruct { id: 1, name: String::from("test"), date: String::from("2020-08-11"),  });
    }
}
