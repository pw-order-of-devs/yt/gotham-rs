use gotham::state::State;

fn version(state: State) -> (State, &'static str) {
    (state, "0.1.0")
}

fn main() {
    let addr = "127.0.0.1:7878";
    println!("Gotham server is running at http://{}", addr);
    gotham::start(addr, || Ok(version))
}

/// #################################
/// ######       tests         ######
/// #################################

#[cfg(test)]
mod tests {
    use super::*;
    use gotham::hyper::StatusCode;
    use gotham::test::TestServer;

    #[test]
    fn receive_version_response() {
        let response = TestServer::new(|| Ok(version)).unwrap()
            .client()
            .get("http://localhost")
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::OK);
        assert_eq!(&response.read_body().unwrap()[..], b"0.1.0");
    }
}